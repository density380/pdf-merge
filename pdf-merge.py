"""Usage:
  pdf-merge.py [options] <file1> <file2>

  -r, --replace  Remove input files after merge.
"""
import os
import sys

from time import strftime
from docopt import docopt
from PyPDF2 import PdfFileMerger

if __name__ == '__main__':
    args = docopt(__doc__)

    merger = PdfFileMerger(strict=False)

    input1 = open(args['<file1>'], "rb")
    input2 = open(args['<file2>'], "rb")

    merger.append(input1)
    merger.append(input2)

    outfname = "Merged_" + strftime("%Y%m%d_%H%M%S") + ".pdf"
    output = open(outfname, "wb")
    merger.write(output)

    if args['--replace']:
        input1.close()
        input2.close()
        try:
            os.remove(args['<file1>'])
            os.remove(args['<file2>'])
        except OSError:
            pass
